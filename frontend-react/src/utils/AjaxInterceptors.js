import React, { useEffect, useCallback, useContext } from 'react';
import { Snackbar } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import ajax from './ajaxHelper';
import { NetworkContext } from '../Contexts/NetworkContext';
import { NETWORK_EVENTS } from '../constants/actionTypes';

const AjaxInterceptors = () => {
  const [state, dispatch] = useContext(NetworkContext);

  const onRequest = useCallback(
    (request) => {
      const updatedRequest = {
        ...request,
        headers: { ...request.headers, 'Content-Type': 'application/json' },
      };
      dispatch({
        type: NETWORK_EVENTS.NETWORK_CALL_REQUEST,
      });
      return updatedRequest;
    },
    [dispatch],
  );

  const onRequestError = useCallback(
    (error) => {
      dispatch({
        type: NETWORK_EVENTS.NETWORK_CALL_FAIL,
        payload: error,
      });
      return Promise.reject(error);
    },
    [dispatch],
  );

  const onResponse = useCallback(
    (response) => {
      if (!response.data.status) {
        dispatch({
          type: NETWORK_EVENTS.NETWORK_CALL_FAIL,
          payload: new Error(response.data.message ?? 'Something went wrong'),
        });
        return Promise.reject(new Error(response.data.message ?? 'Something went wrong'));
      }
      dispatch({
        type: NETWORK_EVENTS.NETWORK_CALL_SUCCESS,
      });
      return response.data;
    },
    [dispatch],
  );

  const onResponseError = useCallback(
    (error) => {
      dispatch({
        type: NETWORK_EVENTS.NETWORK_CALL_FAIL,
        payload: error,
      });
      return Promise.reject(error);
    },
    [dispatch],
  );

  const handleClose = useCallback(() => {
    dispatch({ type: NETWORK_EVENTS.NETWORK_CALL_RESET });
  }, [dispatch]);

  useEffect(() => {
    const requestInterceptor = ajax.interceptors.request.use(onRequest, onRequestError);
    const responseInterceptor = ajax.interceptors.response.use(onResponse, onResponseError);
    return () => {
      ajax.interceptors.request.eject(requestInterceptor);
      ajax.interceptors.response.eject(responseInterceptor);
    };
  }, [onRequest, onRequestError, onResponse, onResponseError]);

  return (
    <Choose>
      <When condition={state.loading && state.count === 0}>
        <span>IfBlock</span>
      </When>
      <When condition={!!state.error && state.count === 0}>
        <Snackbar open={!!state.error} autoHideDuration={6000} onClose={handleClose}>
          <Alert elevation={6} variant="filled" onClose={handleClose} severity="error">
            {state.error.message}
          </Alert>
        </Snackbar>
      </When>
    </Choose>
  );
};

export default AjaxInterceptors;
