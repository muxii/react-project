import axios from 'axios';

const instance = axios.create({
  baseURL: 'http://smartstudio-dev.azul-arc.com/api',
  timeout: 3000,
});

export default instance;
