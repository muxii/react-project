import React from 'react';
import PropTypes from 'prop-types';
import { Typography, Grid } from '@material-ui/core';

const HeaderLabel = ({ header, caption }) => {
  return (
    <>
      <Grid container justify="center">
        <Typography variant="h2">{header}</Typography>
      </Grid>
      {caption && (
        <Grid container justify="center">
          <Typography variant="h4">{caption}</Typography>
        </Grid>
      )}
    </>
  );
};

HeaderLabel.propTypes = {
  header: PropTypes.string.isRequired,
  caption: PropTypes.string,
};

HeaderLabel.defaultProps = {
  caption: '',
};

export default HeaderLabel;
