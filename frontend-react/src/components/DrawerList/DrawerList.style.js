import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => {
  return {
    listItem: {
      borderLeft: (props) => (props.active ? '6px solid #4860CF' : '6px solid #fff'),
      backgroundColor: (props) => (props.active ? 'rgba(72, 96, 207, 0.1)' : '#fff'),
      color: (props) => (props.active ? '#3C3F61' : '#BBBBBB'),
      '&:hover': {
        borderLeft: '6px solid #4860CF',
      },
      '&:active': {
        borderLeft: '6px solid #4860CF',
      },
    },
  };
});

export default useStyles;
