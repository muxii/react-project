/* eslint-disable react-hooks/rules-of-hooks */
import React from 'react';
import PropTypes from 'prop-types';
import { List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { useNavigate, useLocation } from 'react-router-dom';
import useStyles from './DrawerList.style';

const DrawerList = ({ menuItems }) => {
  const navigate = useNavigate();
  const location = useLocation();

  return (
    <List>
      {menuItems.map((item) => {
        const classes = useStyles({ active: item.path === location.pathname });
        return (
          <ListItem
            button
            key={item.text}
            className={classes.listItem}
            onClick={() => navigate(item.path)}
          >
            {item.icon && <ListItemIcon>{item.icon}</ListItemIcon>}
            <ListItemText primary={item.text} />
          </ListItem>
        );
      })}
    </List>
  );
};

DrawerList.propTypes = {
  menuItems: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string.isRequired,
      icon: PropTypes.element,
      path: PropTypes.string.isRequired,
    }),
  ).isRequired,
};

export default DrawerList;
