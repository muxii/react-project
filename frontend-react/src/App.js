import React, { memo, Suspense } from 'react';
import { BrowserRouter } from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import NetworkProvider from './Contexts/NetworkContext';
import AjaxInterceptors from './utils/AjaxInterceptors';
import Route from './Route';

function App() {
  return (
    <BrowserRouter>
      <CssBaseline />
      <Suspense fallback="loading...">
        <NetworkProvider>
          <AjaxInterceptors />
          <Route isAuth={false} />
        </NetworkProvider>
      </Suspense>
    </BrowserRouter>
  );
}

export default memo(App);
