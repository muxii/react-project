import React, { memo } from 'react';
import { useRoutes } from 'react-router-dom';
import Auth from './screens/Auth';
import Login from './screens/Login';
import ForgotPassword from './screens/ForgotPassword';
import ResetPassword from './screens/ResetPassword';

import AdministratorSettings from './screens/AdministratorSettings';

import CompanyDetails from './screens/CompanyDetails';
import ReportingSettings from './screens/ReportingSettings';

import NotFound from './screens/NotFound';
import Main from './screens/Main';

const Route = () => {
  const element = useRoutes([
    {
      path: '/',
      element: <Auth />,
      children: [
        {
          path: '/login',
          element: <Login />,
        },
        {
          path: '/forgotPassword',
          element: <ForgotPassword />,
        },
        {
          path: '/resetPassword',
          element: <ResetPassword />,
        },
      ],
    },
    {
      path: 'main',
      element: <Main />,
      children: [
        {
          path: '/',
          element: <CompanyDetails />,
        },
        {
          path: '/reportingSettings',
          element: <ReportingSettings />,
        },
        {
          path: '/administratorSettings',
          element: <AdministratorSettings />,
        },
      ],
    },

    { path: '*', element: <NotFound /> },
  ]);

  return element;
};

Route.propTypes = {};

export default memo(Route);
