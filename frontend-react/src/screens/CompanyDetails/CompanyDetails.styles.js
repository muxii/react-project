import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  body1: {
    fontSize: 20,
    fontWeight: 400,
    color: '#353755',
  },
}));

export default useStyles;
