import React from 'react';
import {
  Grid,
  TextField,
  /* Typography, */
  Button,
  InputLabel,
  MenuItem,
  Select,
  FormControl,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';

import useStyles from './CompanyDetails.styles';
import HeaderLabel from '../../components/HeaderLabel';
import Form from '../../components/Form';

// import PropTypes from 'prop-types';

const CompanyDetails = () => {
  const { t } = useTranslation();
  const classes = useStyles();
  const [timezone, setTimezone] = React.useState('');
  const [state, setState] = React.useState('');
  const handleChangetimezone = (event) => {
    setTimezone(event.target.value);
  };

  const handleChangestate = (event) => {
    setState(event.target.value);
  };

  const onSubmit = (data) => console.log(data);

  return (
    <div className={classes.root}>
      <Form onSubmit={onSubmit}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <HeaderLabel header={t('Create A New Company')} />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField id="company-name" label="Company Name*" fullWidth />
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormControl fullWidth className={classes.formControl}>
              <InputLabel id="timezone-label">Time Zone*</InputLabel>
              <Select
                labelId="timezone-label"
                id="timezone"
                value={timezone}
                onChange={handleChangetimezone}
              >
                <MenuItem value={1}>1:00 am</MenuItem>
                <MenuItem value={2}>2:00 am</MenuItem>
                <MenuItem value={3}>3:00 am</MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField id="address-line1" label="Address Line 1*" fullWidth />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField id="address-line2" label="Address Line 2*" fullWidth />
          </Grid>
          <Grid item xs={12} sm={4}>
            <TextField id="city" label="City*" fullWidth />
          </Grid>
          <Grid item xs={12} sm={4}>
            <FormControl fullWidth className={classes.formControl}>
              <InputLabel id="state-label">State*</InputLabel>
              <Select labelId="state-label" id="state" value={state} onChange={handleChangestate}>
                <MenuItem value={1}>State1</MenuItem>
                <MenuItem value={2}>State2</MenuItem>
                <MenuItem value={3}>State3</MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={4}>
            <TextField id="zipcode" label="Zipecode*" fullWidth />
          </Grid>
          <Grid item xs={12} sm={4}>
            <TextField id="superadminfirstname" label="Super Admin First Name*" fullWidth />
          </Grid>
          <Grid item xs={12} sm={4}>
            <TextField id="superadminlastname" label="Super Admin Last Name*" fullWidth />
          </Grid>
          <Grid item xs={12} sm={4}>
            <TextField id="superadminphonenumber" label="Super Admin Phone Number*" fullWidth />
          </Grid>
          <Grid item xs={12} sm={4}>
            <TextField id="superadminemail" label="Super Admin Email*" fullWidth />
          </Grid>
          <Grid item xs={12} sm={4}>
            <TextField id="alias" label="Alias*" fullWidth />
          </Grid>
          {/* <Grid item xs={12}>
            <Typography variant="h3" gutterBottom>
              Add Company Logo
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="h3" gutterBottom>
              Create Password
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="body1" className={classes.body1} gutterBottom>
              Password must be at least 8 characters long and contain at least one uppercase letter,
              one lowercase letter, and either a number or a symbol.
            </Typography>
          </Grid>
          <Grid item xs={12} sm={4}>
            <TextField id="password" label="Password*" fullWidth />
          </Grid>
          <Grid item xs={12} sm={4}>
            <TextField id="confirmpassword" label="Confirm Password*" fullWidth />
          </Grid> */}
          <Grid container spacing={3} justify="center">
            {/* <Grid item>
              <Button variant="contained" color="secondary">
                SAVE FOR LATER
              </Button>
            </Grid> */}
            <Grid item>
              <Button variant="contained" color="primary">
                SAVE & SEND INVITE
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Form>
    </div>
  );
};

CompanyDetails.propTypes = {};

export default CompanyDetails;
