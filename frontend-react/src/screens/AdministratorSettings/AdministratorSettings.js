import React from 'react';
// import PropTypes from 'prop-types';
import { Grid, Typography, Paper } from '@material-ui/core';

import useStyles from './AdministratorSettings.styles';

const AdministratorSettings = () => {
  const classes = useStyles();
  return (
    <Grid className={classes.root}>
      <Grid container>
        <Grid container xs={12}>
          <Typography className={classes.h1} variant="h1">
            Administrator Settings
          </Typography>
        </Grid>
        <Grid container>
          <Grid item>
            <Grid container spacing={3}>
              <Grid item xs={12} sm={6} md={3}>
                <Paper className={classes.paper}>
                  <img
                    className={classes.img}
                    src="/images/setting-icons/company-details.jpg"
                    alt="Company Details"
                  />
                  <Typography className={classes.h5} variant="h5">
                    Company Details
                  </Typography>
                </Paper>
              </Grid>
              <Grid item xs={12} sm={6} md={3}>
                <Paper className={classes.paper}>
                  <img
                    className={classes.img}
                    src="/images/setting-icons/reporting-settings.jpg"
                    alt="Reporting Settings"
                  />
                  <Typography className={classes.h5} variant="h5">
                    Reporting Settings
                  </Typography>
                </Paper>
              </Grid>
              <Grid item xs={12} sm={6} md={3}>
                <Paper className={classes.paper}>
                  <img
                    className={classes.img}
                    src="/images/setting-icons/classes-settings.jpg"
                    alt="Classes Settings"
                  />
                  <Typography className={classes.h5} variant="h5">
                    Classes Settings
                  </Typography>
                </Paper>
              </Grid>
              <Grid item xs={12} sm={6} md={3}>
                <Paper className={classes.paper}>
                  <img
                    className={classes.img}
                    src="/images/setting-icons/peak-nonpeak-hours.jpg"
                    alt="Peak/Non Peak Hours"
                  />
                  <Typography className={classes.h5} variant="h5">
                    Peak/Non Peak Hours
                  </Typography>
                </Paper>
              </Grid>
              <Grid item xs={12} sm={6} md={3}>
                <Paper className={classes.paper}>
                  <img
                    className={classes.img}
                    src="/images/setting-icons/client-waiver.jpg"
                    alt="Client Waiver"
                  />
                  <Typography className={classes.h5} variant="h5">
                    Client Waiver
                  </Typography>
                </Paper>
              </Grid>
              <Grid item xs={12} sm={6} md={3}>
                <Paper className={classes.paper}>
                  <img
                    className={classes.img}
                    src="/images/setting-icons/studios.jpg"
                    alt="Studios"
                  />
                  <Typography className={classes.h5} variant="h5">
                    Studios
                  </Typography>
                </Paper>
              </Grid>
              <Grid item xs={12} sm={6} md={3}>
                <Paper className={classes.paper}>
                  <img
                    className={classes.img}
                    src="/images/setting-icons/staff-permissions.jpg"
                    alt="Staff Permissions"
                  />
                  <Typography className={classes.h5} variant="h5">
                    Staff Permissions
                  </Typography>
                </Paper>
              </Grid>
              <Grid item xs={12} sm={6} md={3}>
                <Paper className={classes.paper}>
                  <img
                    className={classes.img}
                    src="/images/setting-icons/staff-payrates.jpg"
                    alt="Staff Payrates"
                  />
                  <Typography className={classes.h5} variant="h5">
                    Staff Payrates
                  </Typography>
                </Paper>
              </Grid>
              <Grid item xs={12} sm={6} md={3}>
                <Paper className={classes.paper}>
                  <img
                    className={classes.img}
                    src="/images/setting-icons/staff-members.jpg"
                    alt="Staff Members"
                  />
                  <Typography className={classes.h5} variant="h5">
                    Staff Members
                  </Typography>
                </Paper>
              </Grid>
              <Grid item xs={12} sm={6} md={3}>
                <Paper className={classes.paper}>
                  <img
                    className={classes.img}
                    src="/images/setting-icons/packages.jpg"
                    alt="Packages"
                  />
                  <Typography className={classes.h5} variant="h5">
                    Packages
                  </Typography>
                </Paper>
              </Grid>
              <Grid item xs={12} sm={6} md={3}>
                <Paper className={classes.paper}>
                  <img
                    className={classes.img}
                    src="/images/setting-icons/fitness-class-templates.jpg"
                    alt="Fitness Class Templates"
                  />
                  <Typography className={classes.h5} variant="h5">
                    Fitness Class Templates
                  </Typography>
                </Paper>
              </Grid>
              <Grid item xs={12} sm={6} md={3}>
                <Paper className={classes.paper}>
                  <img
                    className={classes.img}
                    src="/images/setting-icons/email-settings.jpg"
                    alt="Email Settings"
                  />
                  <Typography className={classes.h5} variant="h5">
                    Email Settings
                  </Typography>
                </Paper>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

AdministratorSettings.propTypes = {};

export default AdministratorSettings;
