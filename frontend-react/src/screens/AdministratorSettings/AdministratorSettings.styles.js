import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  h1: {
    color: '#4860CF',
    fontSize: 24,
    fontWeight: 700,
    marginBottom: 10,
  },
  h5: {
    color: '#353755',
    fontSize: 16,
    fontWeight: 700,
    marginTop: 10,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    minHeight: 190,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    display: 'flex',
    border: '1px solid #F1F2F2',
    borderRadius: 6,
    backgroundColor: '#FFFFFF',
    boxShadow: '0 0 5px 0 rgba(0,0,0,0.1)',
  },
  body1: {
    fontSize: 20,
    fontWeight: 400,
    color: '#353755',
  },
}));

export default useStyles;
