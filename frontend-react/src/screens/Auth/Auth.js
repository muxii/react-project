import React from 'react';
import { Outlet } from 'react-router-dom';
import { Paper, Grid } from '@material-ui/core';
import useStyles from './Auth.styles';

// import PropTypes from 'prop-types';

const Auth = () => {
  const classes = useStyles();
  return (
    <Grid container justify="center" alignItems="center" className={classes.root}>
      <Paper className={classes.paper}>
        <Outlet />
      </Paper>
    </Grid>
  );
};

Auth.propTypes = {};

export default Auth;
