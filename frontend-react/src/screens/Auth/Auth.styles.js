import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    height: '100vh',
    backgroundImage: 'url("/images/banner.jpg")',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center',
    backgroundSize: 'cover',
    backgroundAttachment: 'fixed',
  },
  paper: {
    backgroundColor: 'rgba(255,255,255,0.5)',
    [theme.breakpoints.up('md')]: {
      height: '75vh',
      width: '75vh',
    },
    [theme.breakpoints.down('xs')]: {
      height: '100vh',
      width: '100vh',
    },
    padding: theme.spacing(4),
  },
}));

export default useStyles;
