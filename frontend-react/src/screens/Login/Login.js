import React from 'react';
// import PropTypes from 'prop-types';

import { Grid, Typography, TextField, Button, Link } from '@material-ui/core';
import { useForm, Controller } from 'react-hook-form';
import useStyles from './Login.style';
import useLogin from './useLogin';

const Login = () => {
  const classes = useStyles();
  const { login } = useLogin();
  const { control, handleSubmit } = useForm();
  const onSubmit = (data) => {
    login(data);
  };

  const preventDefault = (event) => event.preventDefault();

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Grid container direction="column" justify="space-between" className={classes.root}>
        <Grid container spacing={3} direction="column">
          <Grid item>
            <Grid container spacing={2} alignItems="center" direction="column">
              <Grid item>
                <img className={classes.img} src="images/studio.png" alt="recipe studio" />
              </Grid>
              <Grid item>
                <Typography variant="h1">Login to your account</Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid item>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <Controller
                  as={TextField}
                  control={control}
                  name="emailId"
                  label="User Name*"
                  fullWidth
                  defaultValue=""
                />
              </Grid>
              <Grid item xs={12}>
                <Controller
                  as={TextField}
                  control={control}
                  name="password"
                  label="Password*"
                  type="password"
                  fullWidth
                  defaultValue=""
                />
              </Grid>
              <Grid item xs={12}>
                <Link className={classes.link} href="#/" onClick={preventDefault}>
                  Forgot password?
                </Link>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid container justify="center">
          <Grid item>
            <Button type="submit" className={classes.button} variant="contained" color="primary">
              LOGIN
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </form>
  );
};

Login.propTypes = {};

export default Login;
