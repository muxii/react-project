import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    height: '100%',
  },
  img: {
    maxWidth: 250,
  },
  link: {
    fontWeight: 700,
  },
  button: {
    width: 300,
  },
  label: {
    fontSize: 20,
    fontWeight: 700,
    color: '#4860CF',
  },
}));

export default useStyles;
