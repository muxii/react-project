import { useCallback, useReducer } from 'react';
import ajax from '../../utils/ajaxHelper';
import authReducer, { initialState } from '../../reducers/loginReducer';
import { AUTH_EVENTS } from '../../constants/actionTypes';

const useLogin = () => {
  const [state, dispatch] = useReducer(authReducer, initialState);

  const login = useCallback(async (data) => {
    dispatch({ type: AUTH_EVENTS.LOGIN_REQUEST });
    try {
      const res = await ajax.post('/login', data, {
        headers: {
          hosttype: 'Company',
        },
      });
      dispatch({ type: AUTH_EVENTS.LOGIN_SUCCESS, payload: res });
    } catch (error) {
      dispatch({ type: AUTH_EVENTS.LOGIN_FAIL, payload: error });
    }
  }, []);

  return {
    login,
    state,
  };
};

export default useLogin;
