import React from 'react';

import { Grid, Typography, TextField, Button } from '@material-ui/core';
import useStyles from './ForgotPassword.style';

const ResetPassword = () => {
  const classes = useStyles();
  return (
    <Grid container direction="column" justify="space-between" className={classes.root}>
      <Grid container spacing={3} direction="column">
        <Grid item>
          <Grid container spacing={2} alignItems="center" direction="column">
            <Grid item>
              <img className={classes.img} src="images/studio.png" alt="recipe studio" />
            </Grid>
            <Grid item>
              <Typography variant="h1">Reset Password</Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                InputLabelProps={{ classes: { root: classes.label } }}
                id="email-address"
                label="Email Address*"
                fullWidth
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Grid container justify="center">
        <Grid item>
          <Button className={classes.button} variant="contained" color="primary">
            SEND ME INSTRUCTIONS
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default ResetPassword;
