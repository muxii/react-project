import React from 'react';
// import PropTypes from 'prop-types';

const NoFound = () => {
  return <div>Page Not found</div>;
};

NoFound.propTypes = {};

export default NoFound;
