import React from 'react';
import { Outlet } from 'react-router-dom';
import { Paper } from '@material-ui/core';
import Nav from '../../components/Nav/Nav';
import useStyles from './Main.styles';

export const menuItems = [
  { text: 'Company Details', path: '/main' },
  { text: 'Reporting Settings', path: '/main/reportingSettings' },
];

const Entry = () => {
  const classes = useStyles();
  return (
    <>
      <Nav menuItems={menuItems} />
      <main>
        <Paper className={classes.paper}>
          <Outlet />
        </Paper>
      </main>
    </>
  );
};

export default Entry;
