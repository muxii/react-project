import { makeStyles } from '@material-ui/core/styles';
import { drawerWidth } from '../../constants/globalConstants';

const useStyles = makeStyles((theme) => {
  return {
    paper: {
      [theme.breakpoints.up('sm')]: {
        marginLeft: drawerWidth + 15,
        marginTop: 15,
        marginRight: 15,
        minHeight: 'calc(100vh - 30px)',
        padding: 30,
      },
      [theme.breakpoints.down('xs')]: {
        marginTop: 48,
        minHeight: 'calc(100vh - 48px)',
        padding: 15,
      },
    },
  };
});

export default useStyles;
