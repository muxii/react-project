import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#4860CF',
      contrastText: '#fff',
    },
    secondary: {
      main: '#4D507B',

      contrastText: '#fff',
    },
  },
  typography: {
    fontFamily: ['Lato', 'sans-serif'].join(','),
    h1: {
      fontSize: 28,
      fontWeight: 700,
      color: '#4D507B',
    },
    h2: {
      fontSize: 24,
      fontWeight: 700,
      color: '#8185B9',
    },
    h3: {
      fontSize: 21,
      fontWeight: 700,
      color: '#4D507B',
    },
    // body1: {
    //   /* fontSize: 20,
    //   fontWeight: 400,
    //   color: '#353755', */
    // },
  },
  overrides: {
    MuiButton: {
      root: {
        borderRadius: 30,
        padding: '11px 35px',
        fontSize: 14,
        fontWeight: 400,
        '@media (max-width:960px)': {
          borderRadius: 20,
          padding: '8px 25px',
          fontSize: 12,
        },
      },
    },
    MuiListItem: {
      button: {
        '&:hover': {
          backgroundColor: 'rgba(72, 96, 207, 0.1)',
          color: '#3C3F61',
        },
        '&:active': {
          backgroundColor: 'rgba(72, 96, 207, 0.1)',
          color: '#3C3F61',
        },
      },
    },
    MuiInputLabel: {
      root: {
        fontSize: 18,
        color: '#96A1BC',
      },
      shrink: {
        fontSize: 20,
        color: '#4860CF',
        fontWeight: 700,
      },
    },
    MuiInputBase: {
      input: {
        fontSize: 18,
        color: '#353755',
      },
    },
    MuiInput: {
      underline: {
        '&&&&:before': {
          borderBottom: '2px solid rgba(187, 187, 187, 1)',
        },
      },
    },
  },
});

export default theme;
