import { NETWORK_EVENTS } from '../constants/actionTypes';

export const initialState = {
  loading: false,
  error: false,
  count: 0,
};

export default (state, { type, payload }) => {
  switch (type) {
    case NETWORK_EVENTS.NETWORK_CALL_REQUEST:
      return { ...state, loading: true, count: state.count + 1 };

    case NETWORK_EVENTS.NETWORK_CALL_SUCCESS:
      return { ...state, loading: false, count: state.count - 1 };

    case NETWORK_EVENTS.NETWORK_CALL_FAIL:
      return { ...state, loading: false, error: payload, count: state.count - 1 };

    case NETWORK_EVENTS.NETWORK_CALL_RESET:
      return initialState;

    default:
      return state;
  }
};
