module.exports = (api) => {
  // This caches the Babel config
  api.cache.using(() => process.env.NODE_ENV);
  return {
    presets: ['@babel/preset-env', '@babel/preset-react'],
    plugins: [
      'jsx-control-statements',
      [
        'i18next-extract',
        {
          outputPath: 'public/locales/{{locale}}/{{ns}}.json',
        },
      ],
      '@babel/plugin-proposal-export-default-from',
      [
        'babel-plugin-import',
        {
          libraryName: '@material-ui/core',
          // Use "'libraryDirectory': ''," if your bundler does not support ES modules
          libraryDirectory: '',
          camel2DashComponentName: false,
        },
        'core',
      ],
      [
        'babel-plugin-import',
        {
          libraryName: '@material-ui/icons',
          // Use "'libraryDirectory': ''," if your bundler does not support ES modules
          libraryDirectory: '',
          camel2DashComponentName: false,
        },
        'icons',
      ],
    ],
  };
};
